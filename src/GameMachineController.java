import java.util.Scanner;

public class GameMachineController {

    public static void main(String[] args) {

        GameMachine gameMachine = new GameMachine();
        boolean display = true;
        Scanner sc = new Scanner(System.in);

        do {
            System.out.println("\n\n       Game Machine Menu: ");
            System.out.println("----------------------------------------");
            System.out.println("1 - Show games");
            System.out.println("2 - Buy a game");
            System.out.println("3 - Exit");
            System.out.print("\nSelect a Menu Option: ");
            try {
                int option = sc.nextInt();
                sc.nextLine();
                switch (option) {
                    case 1:
                        gameMachine.printGames();
                        break;
                    case 2:
                        System.out.println("Enter the title of game you want to buy: ");
                        String gameTitle = sc.nextLine();
                        System.out.println("Amount to pay: ");
                        Double moneyAmount = sc.nextDouble();
                        try {
                            gameMachine.buyAGame(gameTitle, moneyAmount);
                        } catch (GameNotFoundException e) {
                            System.out.println("Game not found: " + e.getMessage());
                        } catch (LowMoneyException e) {
                            System.out.printf("Game cost is: %.2f, you paid only: %.2f%n", e.getGamePrice(), e.getAmount());
                        }

                        break;
                    case 3:
                        display = false;
                        break;
                }
            } catch (NumberFormatException n) {
                n.printStackTrace();

            }
        } while (display == true);

    }
}
