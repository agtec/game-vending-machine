public class GameNotFoundException extends Exception {

    public GameNotFoundException(String gameTitle) {
        super(gameTitle);
    }

}
