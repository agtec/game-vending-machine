import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class GameMachine {

    private List<Game> games = new ArrayList<>();

    public GameMachine() {

        games.addAll(Arrays.asList(new Game("Tomb Raider", 200),
                new Game("Need for Speed", 210.50),
                new Game("The Sims", 178.00),
                new Game("Truck Simulator", 240.99),
                new Game("Grand Theft Auto", 380.70),
                new Game("Unreal", 155.90),
                new Game("The Spore", 273.00)));
    }

    public void printGames() {
        System.out.printf("%16s | %13s%n", "Title", "Price (PLN)");
        System.out.println("--------------------------------------");
        for (Game game : games) {
            System.out.println(game);
        }
    }

    public void buyAGame(String gameTitle, double amountOfMoney) throws GameNotFoundException, LowMoneyException {
        for (Game game : games) {
            if (game.getName().equals(gameTitle)) {
                if (game.getPrice() == amountOfMoney) {
                    System.out.println("Get the game: " + gameTitle);
                } else if (game.getPrice() < amountOfMoney) {
                    System.out.println("Get the game: " + gameTitle);
                    System.out.printf("Take the rest of money: %.2f%n", (amountOfMoney - game.getPrice()));
                } else {
                    throw new LowMoneyException(amountOfMoney, game.getPrice());
                }
            } else {
                throw new GameNotFoundException(gameTitle);
            }
        }
    }
}
