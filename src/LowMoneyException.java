public class LowMoneyException extends Exception{

    double amount;
    double gamePrice;

    LowMoneyException(double amount, double gamePrice) {
        this.amount = amount;
        this.gamePrice = gamePrice;
    }

    public double getAmount() {
        return amount;
    }

    public double getGamePrice() {
        return gamePrice;
    }



}
